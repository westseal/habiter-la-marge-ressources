// variable pour les boutons tags - filtre de navigation
var TAGS_NAV = document.querySelectorAll('.texte_tag_filtre')

// variable pour chaque document
var DOCS = document.querySelectorAll('.document')

// variable pour chaque tag de chaque document
var ITEM_TAGS= document.querySelectorAll('.tag')

// variable pour chaque document qui n'a pas de tags
var NOTAGS= document.querySelectorAll('.notags')

// variable pour se savoir ce qui était sélectionné avant
var selecPrecedente

// pour chaque bouton de filtre
TAGS_NAV.forEach(function(filtre, i) {
	// quand clic sur un bouton filtre
	filtre.addEventListener('click', function() {

  // désélectionner les filtres précédents (on ne peut avoir qu'un seul filtre à la fois)
  document.querySelectorAll('.tag_selection').forEach(function(selected){
    selected.classList.remove('tag_selection')
  })

  // si le filtre est déjà activé, le désactiver
    if (selecPrecedente == filtre) {
      filtre.parentElement.classList.remove('tag_selection')
      DOCS.forEach(function(item) {
        // on réaffiche tout ce qui était caché
        item.classList.remove('desactive')

      })

      // reset la sélection
      selecPrecedente = ''

  } else {
    // enregistre ce qui a été sélectionné
    selecPrecedente = filtre

    // rajoute un style "sélectionné" sur le bouton, ou l'enlève s'il est déjà là
    filtre.classList.toggle('tag_selection')

    // variable texte_filtre contient le texte de ce bouton
    var texte_filtre = filtre.innerHTML

    // cache les documents sans tags
    NOTAGS.forEach(function(notags) {
      notags.parentElement.parentElement.classList.add('desactive')
    })

    // pour chaque tag de document
    ITEM_TAGS.forEach(function(item, i){
       // incluant la variable texte_filtre
      if (item.innerHTML.includes(texte_filtre)){
        // enlève la classe desactive
        item.parentElement.parentElement.classList.remove('desactive')

      }
      else {
        //si non attribue la classe desactive
        item.parentElement.parentElement.classList.add('desactive')

      }
    })
 }
})
})
